#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSettings>
#include <QFile>
#include <QWidget>
#include <QTimer>
#include <QTime>
#include <QDateTime>
#include <QString>
#include <QStringList>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QMessageBox>
#include <QIODevice>
#include <QSettings>
#include <QTableWidgetItem>
#include <QPixmap>
#include <QPainter>
#include <QDebug>
#include <QPushButton>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QDateTime>
#include <QByteArray>
#include <QCloseEvent>
#include <QInputDialog>
#include <QAbstractItemView>
#include <QLatin1Char>
#include <QPalette>

#include "dialog.h"
#include "ui_dialog.h"

#include "command.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

    int baudRate1;  //波特率
    int ReadMessageTime;
    int clock;
    int serialPortStatus;

    QPixmap pic;
    QTimer *RecordTimer = nullptr;
    quint8 SlaveAddr;

    QByteArray bufferArray; //接收报文

    void ShowTime();
    void InitData();
    void DisplayDataToList();
    void SerialPortInfo();
    void RTUReadMessage();
    void timeInformationPrint();
    void timerEvent(QTimerEvent *);
    void WriteCoilData(int , QString );
    void PrintRequstMessage(QString );
    void WriteRegistData(int , QString );
    void StructInitialize(MessageBasicInformation *, QByteArray );
    void AbnorResMess(quint8 , quint8 , int , int );
    void NorResMessSend(MessageBasicInformation *);
    void MessageHistory();
    void closeEvent(QCloseEvent *);
    void paintEvent(QPaintEvent *);

    bool nativeEvent(const QByteArray& , void* , long* );
    bool MessageAnalysis(QByteArray );
    bool CRCTest(MessageBasicInformation *, QByteArray );
    bool AnalysisMessageRead(MessageBasicInformation *, int );
    bool AnalysisMessage0X0F(MessageBasicInformation *, QByteArray );
    bool AnalysisMessage0X10(MessageBasicInformation *, QByteArray );

    QString ByteArrayToString(QByteArray ,int , int );
    QString CRCCheck(QString &);
    quint16 BondTwoUint8ToUint16(quint8 , quint8 );

    QByteArray AbnormalResponseMessage(quint8 , quint8 ,int );
    QByteArray NorResseMessWrite(MessageBasicInformation *);
    QByteArray NorResseMessCoil(MessageBasicInformation *);
    QByteArray NorResseMessRegister(MessageBasicInformation *);
    QByteArray NorResseMessRead(MessageBasicInformation *, QByteArray );


    int ReceivingTime();
private slots:

    void OpenSerialPort();
    void CloseSerialPort();

    void on_historyBtn_clicked();

    void on_clearBtn_clicked();

private:

    QStringList serialNamePort;     //端口号List实例化

    QSerialPort *serialPort;

    QSettings *settings;

    Ui::Widget *ui;

    Dialog *dialog;
};

#endif // WIDGET_H
