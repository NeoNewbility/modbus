#ifndef COMMAND_H
#define COMMAND_H
#include <QWidget>
#include <qabstractnativeeventfilter.h>
#include <windows.h>
#include <QApplication>
#include <QMutex>

#include "dbt.h"

#define WINDOW_HEIGHT 512

#define WINDOW_WIDTH 768

#define WINDOWS_ICO_PATH "../Modbus-RTU-slave/res/C.jfif"

#define BACK_PATH "../Modbus-RTU-slave/res/0.jpg"   //背景图片路径

#define MAX_NUMBER 65535

#define TCP_MAX_LENGTH 259

#define TCP_MIN_LENGTH 12

#define TCP_MBAP_HEADER_LENGTH  6
#define WRITE_COIL_MINNUM 1  //请求报文写入线圈个数的最小值
#define WRITE_COIL_MAXNUM 1968 //请求报人写入线圈个数的最大值
#define REQUEST_MESSAGE_LENGTH_0X01_0X03  12   //0X01或0X03请求报文长度
#define TCP_MBAP_HEADER_LENGTH 6     //TCP 中MBAP的字节长度
#define REQUEST_MESSAGE_LENGTH_Read  12   //0X01/0X03请求报文长度
#define READ_COIL_MINNUM  1 //读取线圈最小值
#define READ_COIL_MAXNUM 2000  //读取线圈最大值
#define ADDRESS_MAX 65535 //线圈和寄存器的地址最大值
#define READ_REGISTER_MINNUM 1  //请求报文读取寄存器个数的最小值
#define READ_REGISTER_MAXNUM 125  //请求报文读取寄存器个数的最大值

#define CLOCK_REFRESH 500  //时钟刷新时间
#define CLOCK_FORMAT "yyyy-MM-dd hh:mm:ss"  //时钟显示格式
#define ADDRESS_MIN 0  //线圈和寄存器的地址最小值
#define ADDRESS_MAX 65535  //线圈和寄存器的地址最大值
#define READ_COIL_MINNUM 1  //请求报文读取线圈个数的最小值
#define READ_COIL_MAXNUM 2000  //请求报文读取线圈个数的最大值
#define READ_REGISTER_MINNUM 1 //请求报文读取寄存器个数的最小值
#define READ_REGISTER_MAXNUM 125 //请求报文读取寄存器个数的最大值
#define WRITE_COIL_MINNUM 1  //请求报文写入线圈个数的最小值
#define WRITE_COIL_MAXNUM 1968  //请求报文写入线圈个数的最大值
#define WRITE_REGISTER_MINNUM 1 //请求报文写入寄存器个数的最小值
#define WRITE_REGISTER_MAXNUM 123 //请求报文写入寄存器个数的最大值
#define READ_MESSAGE_TIME_LENGTH 300  //接收请求报文的时间为300ms
#define RTU_MESSAGE_MAX_BYTE 255 //RTU报文最大长度
#define MINIMUM_MESSAGE_LENGTH 8 //接收到的请求报文最小长度
#define REQUEST_MESSAGE_LENGTH_0X01_0X03 8 //接收到的0X01或0X03请求报文长度
#define ABNORMAL_RESPONSE_LENGTH 5  //异常响应报文长度
#define WRITE_RESPONSE_LENGTH 8 //正常写入响应报文长度

#define WRITE_REGISTER_MINNUM 1  //请求报文写入寄存器个数最小值
#define WRITE_REGISTER_MAXNUM 123 //请求报文写入寄存器个数最大值

#define RTU_MESSAGE_MAX 255 //RTU的最大报文数

//请求报文基础信息结构体
typedef struct MessageBasicInformation
{
    //请求报文长度
    int Size;
    //目的地址
    quint8 SlaveAddress;
    //功能码
    quint8 FuncCode;
    //起始地址
    quint16 BeginAddress;
    //数量项
    quint16 Number;
    //报文的CRC校验码
    QString CrcCheck;
}MessageBasicInformation;

#endif
