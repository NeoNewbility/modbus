#ifndef COMMAND_H
#define COMMAND_H

#define WINDOW_HEIGHT 512

#define WINDOW_WIDTH 768

#define WINDOWS_ICO_PATH "../Modbus-TCP-slave/res/C.jfif"

#define BACK_PATH "../Modbus-TCP-slave/res/0.jpg"   //背景图片路径

#define MAX_NUMBER 65535

#define TCP_MAX_LENGTH 259

#define TCP_MIN_LENGTH 12

#define TCP_MBAP_HEADER_LENGTH  6
#define WRITE_COIL_MINNUM 1  //请求报文写入线圈个数的最小值
#define WRITE_COIL_MAXNUM 1968 //请求报人写入线圈个数的最大值
#define REQUEST_MESSAGE_LENGTH_0X01_0X03  12   //0X01或0X03请求报文长度
#define TCP_MBAP_HEADER_LENGTH 6     //TCP 中MBAP的字节长度
#define REQUEST_MESSAGE_LENGTH_Read  12   //0X01/0X03请求报文长度
#define READ_COIL_MINNUM  1 //读取线圈最小值
#define READ_COIL_MAXNUM 2000  //读取线圈最大值
#define ADDRESS_MAX 65535 //线圈和寄存器的地址最大值
#define READ_REGISTER_MINNUM 1  //请求报文读取寄存器个数的最小值
#define READ_REGISTER_MAXNUM 125  //请求报文读取寄存器个数的最大值

#define WRITE_REGISTER_MINNUM 1  //请求报文写入寄存器个数最小值
#define WRITE_REGISTER_MAXNUM 123 //请求报文写入寄存器个数最大值

#endif
