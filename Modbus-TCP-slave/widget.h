#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QNetworkInterface>
#include <QString>
#include <QPainter>
#include <QHostInfo>
#include <QDateTime>
#include <QPalette>
#include <QCloseEvent>

#include <QFile>      //文件操作
#include <QHostAddress> //IP地址
#include <QMessageBox> //提示对话框
#include <QPainter>

#include <dialog.h>
#include <QSettings>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    //函数
    Widget(QWidget *parent = nullptr);
    ~Widget();
    void Initialize();
    void paintEvent(QPaintEvent *event);
    void closeEvent(QCloseEvent *event);
    void DisplayDataToList();
    void timeInformationPrint();
    void ShowRequestMessage();
    void ShowResponseMessage(QByteArray);
    void AbnormalMessageSend(QByteArray );
    void NormalResponseMessageSend(QByteArray );
    void byteReverse(QString &);
    void WriteData0X0F(int ,QString );
    void ShowCoilsData(QByteArray ,quint16 );
    void ShowRegisterData(QByteArray );
    void UpdateRegistersData(quint16 , quint16 , QString );
    void WriteData0X10(int ,QString );
    void UpdateCoilsData(quint16 ,quint16 ,QString );
    void SlaveAddressDefault();

    bool AnalysisMessageRead(QByteArray );
    bool AnalysisRequestMessage(QByteArray );
    bool AnalysisMessageWrite(QByteArray );

    QString HexByteArrayToHexString(QByteArray ,int ,int);
    QByteArray AbnormalMessageCreate(QByteArray ,quint8 );
    QByteArray GetData0X01(quint16 ,quint16 );
    QByteArray GetData0X03(quint16 ,quint16 );
    QByteArray NormalResponseMessageCreate(QByteArray ,QByteArray );
    quint16 BondTwoUint8ToUint16(quint8 , quint8 );
    QString HexByteArrayToBinString(QByteArray );
    QString HexByteArrayToDecString(QByteArray );


    //数据   只考虑连接一个
    QTcpServer* TCPServer;
    QTcpSocket* TCPSocket; //用来获得连接客户端的SOCKET套接字
    QTcpSocket* TcpSocket;  //客户端
    QString localHostName; //本主机名
    QPixmap pic;

    QString Port;
    QString IP;

    quint8 SlaveAddress;

    bool isconnection;
    bool TcpServerConnectState = false;
    quint8 defaultSlaveAddress = 1;

    //请求报文字节数组
    QByteArray TcpRequestMessage;

private slots:
    void ServerNewConnection();
    void messageLogging();
    void on_Start_clicked();

    void on_Exit_clicked();

    void solt_SendMessage(); //发送消息的槽

    //void RecvMessage(); //接收消息的槽

    void DisConnect();

    void on_pushButton_Sent_clicked();

    void on_pushButton_clicked();

    void on_slot_disconnect_clicked();

    void on_viewLog_clicked();

private:
    Ui::Widget *ui;

    Dialog *dialog;

    QSettings *settings;
};
#endif // Widget_H
