#ifndef MYTHREAD_H
#define MYTHREAD_H


#include "ui_widget.h"
#include "command.h"
#include <QSettings>
#include <QThread>

QT_BEGIN_NAMESPACE
namespace Ui { class MyThread; }
QT_END_NAMESPACE

class MyThread : public QThread
{
    Q_OBJECT
public:
   // MyThread();

    MyThread(QWidget *parent = nullptr){
        Q_UNUSED(parent);
    }


protected:

    void run();

signals:

    void isDone();

public slots:

private slots:



private:

    QSettings *settings;
    Ui::Widget *ui;

};

#endif // MYTHREAD_H
