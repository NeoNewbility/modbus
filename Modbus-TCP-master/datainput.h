#ifndef DATAINPUT_H
#define DATAINPUT_H

#include <QDialog>
#include <QDebug>
#include <QMessageBox>
#include <QDebug>

namespace Ui {
class DataInput;
}

class DataInput : public QDialog
{
    Q_OBJECT

public:
    explicit DataInput(QWidget *parent = nullptr);
    ~DataInput();

    QString getData(QString , QString ,int,int);

private slots:
    void on_pushButton_clicked();

public:
    Ui::DataInput *ui;

};

#endif // DATAINPUT_H
