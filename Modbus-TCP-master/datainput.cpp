#include "datainput.h"
#include "ui_datainput.h"

DataInput::DataInput(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DataInput)
{
    ui->setupUi(this);
    this->setFixedSize(500,300);


    ui->tableWidget->setRowCount(1);

}


QString DataInput::getData(QString title, QString writeGui,int coilInput,int registerInput)
{

    if(coilInput>=1)
    {
        ui->tableWidget->setColumnCount(coilInput);
        ui->tableWidget->setItem(0,coilInput,new QTableWidgetItem(QString("0")));

        for(int j=0;j<coilInput;j++)
        {
            ui->tableWidget->setItem(0,j,new QTableWidgetItem(QString("0")));
        }

        setWindowTitle(title);
        ui->label->setText(writeGui);
        ui->label_2->setText("取消输入或者输入不全则未输入部分补'0',输入超出则自动截取!");
        exec();
        QString null = nullptr;
        QString addStr ;//= this->ui->lineEdit->text();


        for(int test=0;test<coilInput;test++)
        {
            addStr += this->ui->tableWidget->item(0,test)->text();
        }

        return addStr;
    }
    if(coilInput==0)
    {
        ui->tableWidget->setColumnCount(registerInput);
        ui->tableWidget->setItem(0,registerInput,new QTableWidgetItem(QString("0")));

        for(int j=0;j<registerInput;j++)
        {
            ui->tableWidget->setItem(0,j,new QTableWidgetItem(QString("0")));
        }

        setWindowTitle(title);
        ui->label->setText(writeGui);
        ui->label_2->setText("取消输入或者输入不全则未输入部分补'0',输入超出则自动截取!");
        exec();
        QString null = nullptr;
        QString addStr ;//= this->ui->lineEdit->text();


        for(int test=0;test<registerInput;test++)
        {
            if((this->ui->tableWidget->item(0,test)->text().toInt(nullptr,10)>=0) \
                    and(this->ui->tableWidget->item(0,test)->text().toInt(nullptr,10)<=65535))
            {
                addStr +='-'+this->ui->tableWidget->item(0,test)->text();
            }
            else {
                addStr += '-'+'0';
            }
            //Register = writeRegister.toInt(nullptr,10);
        }
        qDebug()<<addStr;

        return addStr;
    }

    return QString(' ');

}

DataInput::~DataInput()
{
    delete ui;
}

void DataInput::on_pushButton_clicked()
{
    this->close();
}
