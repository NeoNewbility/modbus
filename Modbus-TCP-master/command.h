#ifndef COMMAND_H
#define COMMAND_H

#include <QPalette>

#define WINDOW_HEIGHT 512

#define WINDOW_WIDTH 768

#define WINDOWS_ICO_PATH "../Modbus-TCP-master/res/C.jfif"

#define BACK_PATH "../Modbus-TCP-master/res/0.jpg"   //背景图片路径

#define MAX_NUMBER 65535

#define TCP_REQUEST_MESSAGE_LENGTH  12

#define WRITE_REGISTER_MIN 0

#define WRITE_REGISTER_MAX 65535

#define MESSAGE_LENGTH_MIN  9

#define MESSAGE_LENGTH_MAX  259

#define RESEND_MAX  3

#define TIMEOUTLENGTH    5000

typedef struct MessageBasicInformation
{
    //事务元标识符
    quint16 transactionIdenti;
    //协议标识符
    quint16 protocolIdenti;
    //基础长度(6)
    quint16 length;
    //单元标识符
    quint16 slaveAddr;
    //功能码
    quint8 funcCode;
    //起始地址
    quint16 beginAddr;
    //数量
    quint16 num;
}MessageBasicInformation;


#endif
//MessageBasicInformation
//requestMessage   requestMessageArray
//RequestMessage0x010x03Build   RequestMessageStructure
//TimeInformation   timeInformationPrint
//RequestMessage0x0F0x10    writeCoilsAndRegisters
//userCoilsInputProcess     coilsInputProcess
//userRegistersInputProcess registersInputProcess
