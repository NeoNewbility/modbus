#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpSocket> //通信套接字
#include <QFile>      //文件操作
#include <QHostAddress> //IP地址
#include <QMessageBox> //提示对话框
#include <QPainter>
#include <QDateTime>
#include <QtDebug>
#include <QCloseEvent>
#include <QSettings>
#include <QFile>
#include <QDialog>
#include <mythread.h>
#include <QApplication>
#include <Qt>
#include "datainput.h"
#include "dialog.h"

#include "ui_datainput.h"

class MyThread;

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void timeInformationPrint();
    void Initialize();
    void paintEvent(QPaintEvent *);
    void WindowInit();
    void closeEvent(QCloseEvent *);
    void readCoilAndRegisterInfo();
    void byteReverse(QString &);
    void ShowResponseMessage(QString);
    void writeCoilsAndRegisters(int );
    void TCPRequestMessageSend();
    void ExceptionCodeJudge(quint8 );
    void UpdateCoilsData(quint16 ,quint16 ,QString );
    void WriteCoilsData(int , QString );
    void UpdateRegistersData(quint16 , quint16 , QString );
    void WriteRegistersData(int , QString );
    void TemporaryStructInitialize(MessageBasicInformation *,int);
    void structInitialize(MessageBasicInformation *);

    bool FuncCodeProcess0X01(QByteArray , QByteArray );
    bool FuncCodeProcess0X03(QByteArray , QByteArray );
    bool FuncCodeProcess0X0F(QByteArray , QByteArray );
    bool FuncCodeProcess0X10(QByteArray , QByteArray );
    bool coilsValueJudge(QString &);
    bool messageAnalysis(QByteArray);
    bool MessageLegalJudge(QByteArray , QByteArray );
    bool FuncCodeLegalJudge(QByteArray );
    bool ExceptionCodeProcess(QByteArray );


    QByteArray RequestMessageStructureRead(MessageBasicInformation *);
    QByteArray coilsInputProcess();
    QByteArray registersInputProcess();
    QString ByteArrayToString(QByteArray ,int , int );
    QByteArray RequestMessageStructureWrite(MessageBasicInformation *, QByteArray );
    QByteArray TemporarycoilsProcess(QString);


    quint16 BondTwoUint8ToUint16(quint8 , quint8 );

    //数据
    QTcpSocket* TCPSocket;
    //定义请求报文字节数组
    QByteArray requestMessageArray;

    QPixmap pic;
    bool TcpConnectIdentifier = false;
    bool isconnection;
    bool TCPSendIdentifier = false;

    QString IPAddr;
    int port;
    QString res;

    DataInput *input;

    //初始化超时重传次数
    int resendNumber;


private slots:

    void on_link_clicked();
    void slot_connected(); //处理成功连接到服务器的槽
    void slot_sendmessage(); //发送消息到服务器的槽
    void slot_recvmessage(); //接收来自服务器的消息的槽

    void slot_disconnect();

    void on_pushButton_2_clicked();

    void on_slot_disconnect_clicked();  //取消与服务器连接的槽

    void on_pushButton_3_clicked();
    void DisplayDataToList();
    void tableItemClicked(int , int );
    void messageLogging();
    void on_viewLog_clicked();
    void on_requestBtn_clicked(int,int);


private:

    Dialog *dialog;

    QSettings *settings;

    Ui::Widget *ui;
};

#endif
