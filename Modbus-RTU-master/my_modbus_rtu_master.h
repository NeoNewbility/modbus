#ifndef MY_MODBUS_RTU_H
#define MY_MODBUS_RTU_H

#include "widget.h"
#include <iostream>
#include <QString>
#include <QByteArray>
//----------------------------return value------------------------------
#define MESSAGE_LENGTH_OVERLONG 1       //报文过长
#define MESSAGE_LENGTH_HEATING  2       //报文过短
#define SLAVE_ADDRESS_ERROR     3       //从机地址错误
#define MESSAGE_LENGTH_ERROR    4       //报文长度错误
#define ERROR_CODE_ANOMALY      5       //差错吗异常
#define CHCEK_CODE_ERROR        6       //校验码出错
#define FUNCTION_ERROR          7       //功能码错误
#define WITH_REQUEST_MISTAKE    8       //与请求功能码不一致
#define RESPONSE_CHCEK_ERROR    9       //响应报文CRC错误
#define BYTESIZE_MISTAKE        10      //字节大小不一致
#define START_ADDRESS_ERROR     11      //起始地址出错
#define NUMBER_ERROR            12      //查询数量出错

#define DATA_RETURN             100     //数据返回

#define DATA_RETURN_COIL        101     //线圈数据返回
#define DATA_RETURN_REGISTER    102     //寄存器数据返回

class my_modbus_rtu_master
{
public:
    my_modbus_rtu_master();
    QByteArray MessageInformation(QByteArray,QString);        //报文整合

    //报文解析
    int AnalysisResponseMessage(QByteArray ,QByteArray);

    QByteArray messageInfo;
    QString dataPtr;
private:
    QByteArray MessageBuildRead(QByteArray);                                //报文生成
    QByteArray MessageBuildWrite(QByteArray,QByteArray);

    quint16 BondTwoUint8ToUint16(quint8 , quint8 );
    quint16 CRC16Modbus(const QByteArray &,int );

    QByteArray InputCoils(QString );
    QByteArray InputRegisters(QString );

    QString ByteArrayToBinString(QByteArray ,quint16 );
    QString ByteArrayToDecString(QByteArray );
    QString ByteArrayToString(QByteArray ,int , int);

    void byteReverse(QString &);
};

#endif // MY_MODBUS_RTU_H
