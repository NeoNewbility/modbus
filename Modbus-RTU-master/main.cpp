#include "widget.h"
#include "ui_widget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;

    w.setWindowTitle("Modbus RTU Master ");
    QFont f("华文新魏",12);
    a.setFont(f);

    w.show();

    return a.exec();
}
