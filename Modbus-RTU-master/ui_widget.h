/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QTextEdit *dataDisplay;
    QGroupBox *groupBox;
    QPushButton *openSerialPortBtn;
    QPushButton *closeSerialPortBtn;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label;
    QComboBox *serialID;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QComboBox *baudID;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_6;
    QComboBox *dataID;
    QHBoxLayout *horizontalLayout;
    QLabel *label_5;
    QComboBox *stopID;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_4;
    QComboBox *checkID;
    QGroupBox *groupBox_2;
    QPushButton *sendButton;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_7;
    QLineEdit *slaveAddr;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_8;
    QLineEdit *startAddr;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_9;
    QComboBox *SelectFunctionCode;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_10;
    QLineEdit *sendNumber;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_11;
    QLabel *DisplayNowTime;
    QTabWidget *tabWidget;
    QWidget *widget;
    QTableWidget *coilTable;
    QWidget *layoutWidget3;
    QHBoxLayout *horizontalLayout_12;
    QLineEdit *coilLineEdit;
    QPushButton *coilSearchBtn;
    QWidget *widget1;
    QTableWidget *registerTable;
    QWidget *layoutWidget4;
    QHBoxLayout *horizontalLayout_11;
    QLineEdit *registerLineEdit;
    QPushButton *registerSearchBtn;
    QPushButton *dialogBtn;
    QPushButton *clearBtn;
    QPushButton *pushButton;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(768, 512);
        Widget->setMinimumSize(QSize(768, 512));
        Widget->setMaximumSize(QSize(768, 512));
        dataDisplay = new QTextEdit(Widget);
        dataDisplay->setObjectName(QString::fromUtf8("dataDisplay"));
        dataDisplay->setGeometry(QRect(10, 10, 431, 291));
        dataDisplay->setReadOnly(true);
        groupBox = new QGroupBox(Widget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(460, 40, 301, 171));
        openSerialPortBtn = new QPushButton(groupBox);
        openSerialPortBtn->setObjectName(QString::fromUtf8("openSerialPortBtn"));
        openSerialPortBtn->setGeometry(QRect(210, 10, 75, 71));
        closeSerialPortBtn = new QPushButton(groupBox);
        closeSerialPortBtn->setObjectName(QString::fromUtf8("closeSerialPortBtn"));
        closeSerialPortBtn->setGeometry(QRect(210, 90, 75, 71));
        layoutWidget = new QWidget(groupBox);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 20, 181, 136));
        gridLayout_2 = new QGridLayout(layoutWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_5->addWidget(label);

        serialID = new QComboBox(layoutWidget);
        serialID->setObjectName(QString::fromUtf8("serialID"));
        serialID->setMinimumSize(QSize(121, 20));

        horizontalLayout_5->addWidget(serialID);


        gridLayout_2->addLayout(horizontalLayout_5, 0, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_4->addWidget(label_3);

        baudID = new QComboBox(layoutWidget);
        baudID->addItem(QString());
        baudID->addItem(QString());
        baudID->addItem(QString());
        baudID->setObjectName(QString::fromUtf8("baudID"));
        baudID->setMinimumSize(QSize(121, 20));

        horizontalLayout_4->addWidget(baudID);


        gridLayout_2->addLayout(horizontalLayout_4, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_3->addWidget(label_6);

        dataID = new QComboBox(layoutWidget);
        dataID->addItem(QString());
        dataID->addItem(QString());
        dataID->addItem(QString());
        dataID->addItem(QString());
        dataID->setObjectName(QString::fromUtf8("dataID"));
        dataID->setMinimumSize(QSize(121, 20));

        horizontalLayout_3->addWidget(dataID);


        gridLayout_2->addLayout(horizontalLayout_3, 2, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout->addWidget(label_5);

        stopID = new QComboBox(layoutWidget);
        stopID->addItem(QString());
        stopID->addItem(QString());
        stopID->addItem(QString());
        stopID->setObjectName(QString::fromUtf8("stopID"));
        stopID->setMinimumSize(QSize(121, 20));

        horizontalLayout->addWidget(stopID);


        gridLayout_2->addLayout(horizontalLayout, 3, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_2->addWidget(label_4);

        checkID = new QComboBox(layoutWidget);
        checkID->addItem(QString());
        checkID->addItem(QString());
        checkID->addItem(QString());
        checkID->setObjectName(QString::fromUtf8("checkID"));
        checkID->setMinimumSize(QSize(121, 20));

        horizontalLayout_2->addWidget(checkID);


        gridLayout_2->addLayout(horizontalLayout_2, 4, 0, 1, 1);

        groupBox_2 = new QGroupBox(Widget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(460, 210, 301, 141));
        sendButton = new QPushButton(groupBox_2);
        sendButton->setObjectName(QString::fromUtf8("sendButton"));
        sendButton->setGeometry(QRect(220, 20, 75, 111));
        layoutWidget1 = new QWidget(groupBox_2);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 20, 201, 108));
        gridLayout = new QGridLayout(layoutWidget1);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_7 = new QLabel(layoutWidget1);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_6->addWidget(label_7);

        slaveAddr = new QLineEdit(layoutWidget1);
        slaveAddr->setObjectName(QString::fromUtf8("slaveAddr"));
        slaveAddr->setMinimumSize(QSize(121, 20));
        slaveAddr->setMaximumSize(QSize(121, 20));

        horizontalLayout_6->addWidget(slaveAddr);


        gridLayout->addLayout(horizontalLayout_6, 0, 0, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_8 = new QLabel(layoutWidget1);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_7->addWidget(label_8);

        startAddr = new QLineEdit(layoutWidget1);
        startAddr->setObjectName(QString::fromUtf8("startAddr"));
        startAddr->setMinimumSize(QSize(121, 20));
        startAddr->setMaximumSize(QSize(121, 20));

        horizontalLayout_7->addWidget(startAddr);


        gridLayout->addLayout(horizontalLayout_7, 1, 0, 1, 1);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_9 = new QLabel(layoutWidget1);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_8->addWidget(label_9);

        SelectFunctionCode = new QComboBox(layoutWidget1);
        SelectFunctionCode->addItem(QString());
        SelectFunctionCode->addItem(QString());
        SelectFunctionCode->addItem(QString());
        SelectFunctionCode->addItem(QString());
        SelectFunctionCode->setObjectName(QString::fromUtf8("SelectFunctionCode"));
        SelectFunctionCode->setMinimumSize(QSize(121, 20));

        horizontalLayout_8->addWidget(SelectFunctionCode);


        gridLayout->addLayout(horizontalLayout_8, 2, 0, 1, 1);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_10 = new QLabel(layoutWidget1);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_9->addWidget(label_10);

        sendNumber = new QLineEdit(layoutWidget1);
        sendNumber->setObjectName(QString::fromUtf8("sendNumber"));
        sendNumber->setMinimumSize(QSize(121, 20));
        sendNumber->setMaximumSize(QSize(121, 20));

        horizontalLayout_9->addWidget(sendNumber);


        gridLayout->addLayout(horizontalLayout_9, 3, 0, 1, 1);

        layoutWidget2 = new QWidget(Widget);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(550, 10, 211, 22));
        horizontalLayout_10 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        label_11 = new QLabel(layoutWidget2);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_10->addWidget(label_11);

        DisplayNowTime = new QLabel(layoutWidget2);
        DisplayNowTime->setObjectName(QString::fromUtf8("DisplayNowTime"));
        DisplayNowTime->setMinimumSize(QSize(150, 20));

        horizontalLayout_10->addWidget(DisplayNowTime);

        tabWidget = new QTabWidget(Widget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(10, 340, 751, 171));
        tabWidget->setMouseTracking(false);
        tabWidget->setDocumentMode(false);
        tabWidget->setTabsClosable(false);
        tabWidget->setMovable(false);
        tabWidget->setTabBarAutoHide(false);
        widget = new QWidget();
        widget->setObjectName(QString::fromUtf8("widget"));
        coilTable = new QTableWidget(widget);
        coilTable->setObjectName(QString::fromUtf8("coilTable"));
        coilTable->setGeometry(QRect(10, 30, 731, 111));
        coilTable->setSortingEnabled(false);
        layoutWidget3 = new QWidget(widget);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(280, 0, 216, 25));
        horizontalLayout_12 = new QHBoxLayout(layoutWidget3);
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(0, 0, 0, 0);
        coilLineEdit = new QLineEdit(layoutWidget3);
        coilLineEdit->setObjectName(QString::fromUtf8("coilLineEdit"));

        horizontalLayout_12->addWidget(coilLineEdit);

        coilSearchBtn = new QPushButton(layoutWidget3);
        coilSearchBtn->setObjectName(QString::fromUtf8("coilSearchBtn"));

        horizontalLayout_12->addWidget(coilSearchBtn);

        tabWidget->addTab(widget, QString());
        tabWidget->setTabText(tabWidget->indexOf(widget), QString::fromUtf8("\347\272\277\345\234\210"));
        widget1 = new QWidget();
        widget1->setObjectName(QString::fromUtf8("widget1"));
        registerTable = new QTableWidget(widget1);
        registerTable->setObjectName(QString::fromUtf8("registerTable"));
        registerTable->setGeometry(QRect(10, 30, 731, 111));
        layoutWidget4 = new QWidget(widget1);
        layoutWidget4->setObjectName(QString::fromUtf8("layoutWidget4"));
        layoutWidget4->setGeometry(QRect(280, 0, 216, 25));
        horizontalLayout_11 = new QHBoxLayout(layoutWidget4);
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(0, 0, 0, 0);
        registerLineEdit = new QLineEdit(layoutWidget4);
        registerLineEdit->setObjectName(QString::fromUtf8("registerLineEdit"));

        horizontalLayout_11->addWidget(registerLineEdit);

        registerSearchBtn = new QPushButton(layoutWidget4);
        registerSearchBtn->setObjectName(QString::fromUtf8("registerSearchBtn"));

        horizontalLayout_11->addWidget(registerSearchBtn);

        tabWidget->addTab(widget1, QString());
        dialogBtn = new QPushButton(Widget);
        dialogBtn->setObjectName(QString::fromUtf8("dialogBtn"));
        dialogBtn->setGeometry(QRect(370, 310, 75, 23));
        clearBtn = new QPushButton(Widget);
        clearBtn->setObjectName(QString::fromUtf8("clearBtn"));
        clearBtn->setGeometry(QRect(470, 10, 75, 23));
        pushButton = new QPushButton(Widget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(290, 310, 75, 23));

        retranslateUi(Widget);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", nullptr));
        groupBox->setTitle(QApplication::translate("Widget", "\344\270\262\345\217\243", nullptr));
        openSerialPortBtn->setText(QApplication::translate("Widget", "\346\211\223\345\274\200\344\270\262\345\217\243", nullptr));
        closeSerialPortBtn->setText(QApplication::translate("Widget", "\345\205\263\351\227\255\344\270\262\345\217\243", nullptr));
        label->setText(QApplication::translate("Widget", "\344\270\262  \345\217\243:", nullptr));
        label_3->setText(QApplication::translate("Widget", "\346\263\242\347\211\271\347\216\207:", nullptr));
        baudID->setItemText(0, QApplication::translate("Widget", "115200", nullptr));
        baudID->setItemText(1, QApplication::translate("Widget", "9600", nullptr));
        baudID->setItemText(2, QApplication::translate("Widget", "4800", nullptr));

        label_6->setText(QApplication::translate("Widget", "\346\225\260\346\215\256\344\275\215:", nullptr));
        dataID->setItemText(0, QApplication::translate("Widget", "8", nullptr));
        dataID->setItemText(1, QApplication::translate("Widget", "7", nullptr));
        dataID->setItemText(2, QApplication::translate("Widget", "6", nullptr));
        dataID->setItemText(3, QApplication::translate("Widget", "5", nullptr));

        label_5->setText(QApplication::translate("Widget", "\345\201\234\346\255\242\344\275\215:", nullptr));
        stopID->setItemText(0, QApplication::translate("Widget", "1", nullptr));
        stopID->setItemText(1, QApplication::translate("Widget", "2", nullptr));
        stopID->setItemText(2, QApplication::translate("Widget", "1.5", nullptr));

        label_4->setText(QApplication::translate("Widget", "\346\240\241\351\252\214\344\275\215:", nullptr));
        checkID->setItemText(0, QApplication::translate("Widget", "none", nullptr));
        checkID->setItemText(1, QApplication::translate("Widget", "\345\245\207\346\240\241\351\252\214", nullptr));
        checkID->setItemText(2, QApplication::translate("Widget", "\345\201\266\346\240\241\351\252\214", nullptr));

        groupBox_2->setTitle(QApplication::translate("Widget", "\346\212\245\346\226\207", nullptr));
        sendButton->setText(QApplication::translate("Widget", "\345\217\221  \351\200\201", nullptr));
        label_7->setText(QApplication::translate("Widget", "\344\273\216\346\234\272\345\234\260\345\235\200:", nullptr));
        label_8->setText(QApplication::translate("Widget", "\350\265\267\345\247\213\345\234\260\345\235\200:", nullptr));
        label_9->setText(QApplication::translate("Widget", "\345\212\237\350\203\275\347\240\201:", nullptr));
        SelectFunctionCode->setItemText(0, QApplication::translate("Widget", "0x01", nullptr));
        SelectFunctionCode->setItemText(1, QApplication::translate("Widget", "0x03", nullptr));
        SelectFunctionCode->setItemText(2, QApplication::translate("Widget", "0x0f", nullptr));
        SelectFunctionCode->setItemText(3, QApplication::translate("Widget", "0x10", nullptr));

        label_10->setText(QApplication::translate("Widget", "\346\225\260\351\207\217:", nullptr));
        sendNumber->setText(QString());
        label_11->setText(QApplication::translate("Widget", "\345\275\223\345\211\215\346\227\266\351\227\264", nullptr));
        DisplayNowTime->setText(QString());
        coilSearchBtn->setText(QApplication::translate("Widget", "\346\220\234\347\264\242", nullptr));
        registerSearchBtn->setText(QApplication::translate("Widget", "\346\220\234\347\264\242", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(widget1), QApplication::translate("Widget", "\345\257\204\345\255\230\345\231\250", nullptr));
        dialogBtn->setText(QApplication::translate("Widget", "\345\216\206\345\217\262\350\256\260\345\275\225", nullptr));
        clearBtn->setText(QApplication::translate("Widget", "Clear", nullptr));
        pushButton->setText(QApplication::translate("Widget", "\344\270\262\345\217\243\345\210\267\346\226\260", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
