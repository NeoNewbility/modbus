#ifndef WIDGET_H
#define WIDGET_H

#include "my_modbus_rtu_master.h"

#include <QWidget>
#include <QTimer>
#include <QTime>
#include <QDateTime>
#include <QString>
#include <QStringList>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QMessageBox>
#include <QIODevice>
#include <QSettings>
#include <QTableWidgetItem>
#include <QPixmap>
#include <QDebug>
#include <QPushButton>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QDateTime>
#include <QByteArray>
#include <QCloseEvent>
#include <QInputDialog>
#include <QAbstractItemView>
#include <QLatin1Char>
#include <QPainter>
#include <qabstractnativeeventfilter.h>
#include <windows.h>
#include <QApplication>
#include <QMutex>

#include "dbt.h"
#include "dialog.h"
#include "ui_dialog.h"
#include "datainput.h"
#include "ui_datainput.h"

//请求报文结构
typedef struct RequestMessageStruct
{
    //地址
    quint8 slaveAddress;
    //功能码
    quint8 functionCode;
    //起始地址
    quint16 beginAddress;
    //数量
    quint16 dataNumber;
}RequestMessageStruct;

namespace Ui {
class Widget;
}

class my_modbus_rtu_master;
class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

    void SerialPortInfo();

    void ShowAllWindow();

    void DisplayDataToList();
    void RequestMessageInit(int);

    void RequestMessageStructInitialization();
    void closeEvent(QCloseEvent *);
    void ShowResponseMessage(QByteArray );
    void paintEvent(QPaintEvent *);

    void byteReverse(QString &);

    void timerEvent(QTimerEvent *);
    void MessageHistory();
    bool nativeEvent(const QByteArray& , void* , long* );
    bool ParseResponseMessage(QByteArray);

    void WriteCoilsData(int Column, QString );

    quint16 BondTwoUint8ToUint16(quint8 , quint8 );

    QString HexByteArrayToHexString(QByteArray ,int , int );

    QString HexByteArrayToBinString(QByteArray ,quint16 );

    QString HexByteArrayToDecString(QByteArray );

    void UpdateCoilsData(quint16 , quint16 , QString );
    QByteArray TemporaryInputCoils(QString);

    void UpdateRegistersData(quint16 , quint16 , QString );
    void ResendRequestMessage();
    void WriteRegistersData(int , QString );

    int ReceivingTime();

    QString InputRegisters();

    //线圈输入
    QString InputCoils();
    QByteArray RequestMessageBuild0X010X03(RequestMessageStruct *);
    QByteArray RequestMessageBuild0X0f0X10(RequestMessageStruct *,QByteArray );


    int timer;
    //报文读取计时器
    int ReadMessageTimer;
    int ResendNumber = 0;

    QString res;

    int connectSucceed = 0;
    //背景图
    QPixmap pic;
    //报文重发计时器
    QTimer *ResendMessageTimer;
    bool isReceiveResponseMessage;
    DataInput *input;
    QByteArray ReceiveMessageArray;
    QByteArray RequestMessageArray;
    quint16 CRC16Modbus(const QByteArray &data,int flag);

    void TimeShow();

public slots:
    void ShowNowTime();     //显示当前时间
    void OpenSerialPort();
    void SerialPortReadSolt(void);
    void CloseSerialPort();
    void OnSendDataClicked();
    void tableItemClicked(int,int);

    void RTUGetInformation();

    void RTURequestInformation(int,int);


private slots:
    void on_clearBtn_clicked();

    void on_dialogBtn_clicked();

private:
    Ui::Widget *ui;

    QStringList serialNamePort;     //端口号List实例化
    QSerialPort *serialPort;
    QSettings *settings;

    RequestMessageStruct *requestmessage;

    Dialog *dialog;

    HWND m_hwnd;

    my_modbus_rtu_master * modbusRTU;

};

#endif // WIDGET_H
